# PHP-FPM base container

A php-fpm container to use in development environments that require PHP running with Nginx webserver.

Based off php:7.4-fpm image.
